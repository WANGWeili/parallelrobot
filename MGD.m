function F = MDG(x,L1,L2,L3,R,rho)

% Grace a L1, L2 et L3 on d�duit 
% la position de l'organe terminal

xc = x(1);
yc = x(2);
zc = x(3);
alpha = x(4);
beta = x(5);
gamma = x(6);

nx = cos(beta)*cos(gamma);
ny = cos(beta)*sin(gamma);
nz = -sin(beta);

ox = sin(alpha)*sin(beta)*cos(gamma)-cos(alpha)*sin(gamma);
oy = sin(alpha)*sin(beta)*sin(gamma)+cos(alpha)*cos(gamma);
oz = sin(alpha)*cos(beta);

Xc = xc/R;
Yc = yc/R;
Zc = zc/R;

% 6.7
F(1) = - L1^2 + (nx*rho + Xc -1)^2 + ...
        + (ny*rho + Yc)^2 +(nz*rho+ Zc)^2;

% 6.8
F(2) = -L2^2 + (1/4)*(( -nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+...
                      (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
                      (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2 );

% 6.9
F(3) = -L3^2 + (1/4)*(( -nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+...
                      (-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2+...
                      (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2 );
                  
F(4) = ny*rho+Yc;                             % 6.14
F(5) = ny - ox;                               % 6.17
F(6) = 1/2*rho*(nx-oy) - Xc ;                 % 6.18


    
    
    
    
    
    
    
    
    
    
    
    