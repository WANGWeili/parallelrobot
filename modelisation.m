% clc
% clear all 
% tic
% 
% %-----------------------------Robot Parames--------------------------------
% r = 10e-3;% radius of Moving Platform at mm
% R = 10e-3;% radius of Base Platform at mm
% 
% % Ball joints pos in Moving Platform frame{1}
% Pb1_1 = [r; 0; 0];
% Pb2_1 = [-r/2; sqrt(3)*r/2; 0];
% Pb3_1 = [-r/2; -sqrt(3)*r/2; 0];
% % Pin joints pos in Base Platform frame{0}
% Pp1_0 = [R; 0; 0];
% Pp2_0 = [-R/2; sqrt(3)*R/2; 0];
% Pp3_0 = [-R/2; -sqrt(3)*R/2; 0];
% 
% % Rotation Matrix between {0} and {1}
% R = [nx, ox, ax;
%      ny, oy, ay;
%      nz, oz, az];
% % Translation between {0} and {1}
% P = [xc; yc; zc];
% % Transform Matrix between {0} and {1}
% T0_1 = [R, P;
%         0,0,1];
% s

%-----------------------------Pos Init-------------------------------------
dx = 25e-06;
dy = 25e-06;
dz = 25e-06;
l = 180e-03;
R = 1e-02;
r = 1e-02;
zc0 = 1e-02;
P_01 = [0;0;zc0]; % 6.6

x0 = ones(1,12)*10^-3;
result = fsolve(@(x)func(x,dx,dy,dz,l,R,zc0),x0)
T01(:,1) = [result(1),result(2),result(3),0]';
T01(:,2) = [result(4),result(5),result(6),0]';
T01(:,3) = [result(7),result(6),result(9),0]';
T01(:,4) = [result(10),result(11),result(12),1]';

nx = T01(1,1); ny = T01(2,1); nz = T01(3,1);
ox = T01(1,2); oy = T01(2,2); oz = T01(3,2);
ax = T01(1,3); ay = T01(2,3); az = T01(3,3);
xc = T01(1,4); yc = T01(2,4); zc = T01(3,4);

r = 1e-02;
R = 1e-02;

Xc = xc/R; Yc = yc/R; Zc = zc/R;
rho = r/R;

L1 = sqrt( (nx + Xc -1)^2 + (ny+Yc)^2 + (nz + Zc)^2 );

L2 = sqrt( (1/4)*(  ( -nx*rho+ sqrt(3)*ox*rho+ 2*Xc + 1 )^2 + ...
                    ( -ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3) )^2 + ...
                    ( -nz*rho+sqrt(3)*oz*rho+2*Zc )^2              ));
                
L3 = sqrt( (1/4)*(  ( -nx*rho- sqrt(3)*ox*rho+ 2*Xc + 1 )^2 + ...
                    ( -ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3) )^2 + ...
                    ( -nz*rho-sqrt(3)*oz*rho+2*Zc )^2              ));
l1 = R*L1;
l2 = R*L2;
l3 = R*L3;



%% 6.4 Forward Kinematics & Workspace Analysis

x0 = ones(1,3)*pi;
result = fsolve(@(x)func2(x,L1,L2,L3,rho),x0)

t1 = result(1);
t2 = result(2);
t3 = result(3);

% 6.55 | 6.56 | 6.57
B1 = [1-L1*cos(t1); 0 ; L1*sin(t1)];
B2 = [(-1/2)*(1-L2*cos(t2)); (sqrt(3)/2)*(1-L2*cos(t2)); L2*sin(t2) ];
B3 = [(-1/2)*(1-L3*cos(t3)); (-sqrt(3)/2)*(1-L3*cos(t3)); L3*sin(t3)];
           
% 6.58 | 6.59 | 6.60
CENTROID = [ (1/3)*(B1(1)+B2(1)+B3(1))/R;...
             (1/3)*(B1(2)+B2(2)+B3(2))/R;...
             (1/3)*(B1(3)+B2(3)+B3(3))/R];

% 6.61 
P_01B1 = T01(1:3,1:3)*B1 - P_01;
P_01B2 = T01(1:3,1:3)*B2 - P_01;
P_01B3 = T01(1:3,1:3)*B3 - P_01;

% 6.62
Ptip_12 = l*cross(P_01B1,P_01B2)/norm(cross(P_01B1,P_01B2))
Ptip_13 = l*cross(P_01B1,P_01B3)/norm(cross(P_01B1,P_01B3))
Ptip_23 = l*cross(P_01B2,P_01B3)/norm(cross(P_01B2,P_01B3))
Ptip_21 = l*cross(P_01B2,P_01B1)/norm(cross(P_01B2,P_01B1))
Ptip_31 = l*cross(P_01B3,P_01B1)/norm(cross(P_01B3,P_01B1))
Ptip_32 = l*cross(P_01B3,P_01B2)/norm(cross(P_01B3,P_01B2))






