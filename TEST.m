close all
clear all
clc
tic
%% Dans cette partie, on calcule suivant differents L1 L2 L3
dx = 60e-06;
dy = 60e-06;
dz = 60e-06;
l = 60e-03;
R = 1e-02;
r = 1e-02;
zc0 = 1e-02;
P_01 = [0;0;zc0]; % 6.6

rho = r/R;
nb = 10; % nombre de points par longeurs
values = linspace(30,60,nb)*1e-2;
% Calcul de toutes les possiblités (contraintes non respectées)
count = 1;
for i = 1:nb
    for j = 1:nb
        for k = 1:nb
            L1(count)=values(k);
            L2(count)=values(j);
            L3(count)=values(i);
            count = count + 1;
        end
    end
end

Ptip = zeros(6,nb^3);
x0 = ones(1,6);
for i = 1:nb^3
    Ptip(:,i) = ...
        fsolve(@(x)MGD(x,L1(i),L2(i),L3(i),R,rho),x0);
end
%%
figure (1) % plot Ptip X,Y,Z
plot3(Ptip(1,:),Ptip(2,:),Ptip(3,:),'h')
xlabel('X axe')
ylabel('Y axe')
zlabel('Z axe')
ax = gca;
ax.XColor = 'r'; % Red
ax.YColor = 'g'; % Green
ax.ZColor = 'b'; % Blue
%axis([-5e-7 5e-7 -5e-7 5e-7 0 7e-4])

figure (2) % plot Ptip X,Z
plot(Ptip(1,:),Ptip(3,:),'h')
xlabel('X axe')
ylabel('Z axe')
ax = gca;
ax.XColor = 'r'; % Red
ax.YColor = 'b'; % Blue
%axis([-5e-7 5e-7 0 7e-4])

figure (3) % plot Ptip Y,Z
plot(Ptip(2,:),Ptip(3,:),'h')
xlabel('Y axe')
ylabel('Z axe')
ax = gca;
ax.XColor = 'g'; % Green
ax.YColor = 'b'; % Blue
%axis([-5e-7 5e-7 0 7e-4])

toc